import 'package:book/book.dart' as book;

mixin Read{
  void read(){
    print("read");
  }
}
mixin Write{
  void write(){
    print("write");
  }
}

mixin Novel{
  void novel(){
    print("Book novel");
  }
}

class Documentary with Read, Write {
 void PrintBookDocumentary(){
    print("Book Documentary");
    
 }

}

class Journal with Read{
  void PrintJournal(){
     print("Book Journal");
  }

}


class Fiction with Novel{
  void PrintFiction(){
     print("Book Fiction");
     novel();
  }
}
class Book with Read, Write {
  void book(){
    print("Book");
    read();
    write();
  }

 }




void main() {
  Book book0 = new Book();
  book0.book();
  print("---------------------------");
  Documentary book1 = new Documentary();
  book1.PrintBookDocumentary();
  book1.read();
  book1.write();
  print("---------------------------");

  Journal book2 = new Journal();
  book2.PrintJournal();
  book2.read();
  print("---------------------------");

  Fiction book3 = new Fiction();
  book3.PrintFiction();
  print("---------------------------");

  




}
